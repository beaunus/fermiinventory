class Item < ActiveRecord::Base
  belongs_to :user
  validates :name, length: { maximum: 64 }, presence: true,
                  uniqueness: true
end
