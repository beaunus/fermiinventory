require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Fermi Inventory App"
    assert_equal full_title("Help"), "Help | Fermi Inventory App"
  end
end