require 'test_helper'

class ItemTest < ActiveSupport::TestCase

  def setup
    @item = Item.new(name: "Example Item")
  end
  
  test "should be valid" do
    assert @item.valid?
  end
  
  test "name should be present" do
    @item.name = ""
    assert_not @item.valid?
  end
  
  test "name should not be too long" do
    @item.name = "a" * 65
    assert_not @item.valid?
  end
  
  test "name should be unique" do
    duplicate_item = @item.dup
    @item.save
    assert_not duplicate_item.valid?
  end
end
