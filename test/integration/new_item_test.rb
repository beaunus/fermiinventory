require 'test_helper'

class NewItemTest < ActionDispatch::IntegrationTest

  test "invalid item information" do
    get newitem_path
    assert_no_difference 'User.count' do
      post items_path, user: {  name: "",
                                email: "user@invalid",
                                password: "foo",
                                password_confirmation: "bar" }
    end
    assert_template 'items/new'
    assert_select 'div#<CSS id for error explanation>'
    assert_select 'div.<CSS class for field with error'
  end
  
  test "valid item information" do
    get newitem_path
    name    = "Example User"
    user_id = "Beau Harrison"
    assert_difference 'Item.count', 1 do
      post_via_redirect items_path, user: { name:     name,
                                            user_id:  user_id }
    end
    assert_template 'items/show'
    assert_not flash.nil?
  end
end
